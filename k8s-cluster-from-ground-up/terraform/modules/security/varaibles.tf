variable "cluster_name" {
  type        = string
  description = "Name of the cluster"
}

variable "vpc_id" {
  type        = string
  description = "VPC ID"
}
- Generate POD Manifest YAML file (-o yaml). Don't create it(--dry-run)

`kubectl run nginx --image=nginx --dry-run=client -o yaml`

- Generate Deployment YAML file (-o yaml). Don't create it(--dry-run) with 4 Replicas (--replicas=4)

`kubectl create deployment --image=nginx nginx --dry-run=client -o yaml > nginx-deployment.yaml`

aws eks --region us-east-2 update-kubeconfig --name education-eks-34fbt8er

kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode ; echo 

kubectl get secret gitlab-minio-secret -ojsonpath='{.data.accesskey}' | base64 --decode ; echo 

kubectl get secret gitlab-minio-secret -ojsonpath='{.data.secretkey}' | base64 --decode ; echo 

aws eks --region eu-west-2 update-kubeconfig --name sandbox-terraform

aws eks --region eu-west-1 update-kubeconfig --name education-eks-34fbt8er

helm install vault hashicorp/vault --namespace sharedtooling -f override-values.yaml
kubectl get pods --selector='app.kubernetes.io/name=vault' --namespace='sharedtooling'

kubectl create secret 

kubectl edit cm aws-auth -n kube-system

kubectl create secret generic gitlab-initial-root-password --from-file=rootpassword.txt 

kubectl create secret generic gitlab-license --from-file=license=license.txt

alias kc=’kubectl’
alias kgp=’kubectl get pods’
alias kgs=’kubectl get svc’
alias kgc=’kubectl get componentstatuses’
alias kctx=’kubectl config current-context’
alias kcon=’kubectl config use-context’
alias kgc=’kubectl config get-context’


kubectl expose deployment hr-web-app --type=NodePort --port=8080 --name=hr-web-app-service --dry-run=client -o yaml > hr-web-app-service.yaml


kubectl get nodes -o jsonpath='{.items[*].status.nodeInfo.osImage}' > /opt/outputs/nodes_os_x43kj56.txt

set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab

ETCDCTL_API=3 etcdctl snapshot save /opt/etcd-backup.db --endpoints=https://127.0.0.1:2379 --cacert=/etc/kubernetes/pki/ca.crt --cert=/etc/kubernetes/pki/apiserver-etcd-client.crt --key=/etc/kubernetes/pki/apiserver-etcd-client.key \
--insecure-skip-tls-verify

apiVersion: v1
kind: Pod
metadata:
  name: redis-storage
spec:
  containers:
  - image: redis:alpine
    name: redis-storage
    volumeMounts:
    - mountPath: /data/redis
      name: redis-volume
  volumes:
  - name: redis-volume
    emptyDir: {}

apiVersion: v1
kind: Pod
metadata:
  name: ubuntu-sleeper
  namespace: default
spec:
  containers:
  - command:
    - sleep
    - "4800"
    image: ubuntu
    securityContext:
     runAsUser: 1010
     capabilities:
        add: ["SYS_TIME"]
    name: ubuntu